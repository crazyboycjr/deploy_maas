#!/bin/bash

source ./util_functions

# download the tarball
wget -nc http://mvapich.cse.ohio-state.edu/download/mvapich/mv2/mvapich2-2.3.tar.gz
wget -nc https://raw.githubusercontent.com/crazyboycjr/deps/master/build/mvapich2-2.3.patch
tar xf mvapich2-2.3.tar.gz
patch -p0 < mvapich2-2.3.patch
cd mvapich2-2.3

# build & install
./configure --enable-error-messages=all --disable-mcast --disable-fortran --prefix=/usr
make -j 20
sudo make install

# check mpi is successfully installed
echo -e '#include <mpi.h>\nint main() {}\n' | g++ -E -xc++ - 2>&1 >/dev/null
if [ $? -ne 0 ]; then
	error "mpi install failed"
	exit 1
else
	info "mpi installation done"
fi

wget -nc http://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-5.4.3.tar.gz
tar xf osu-micro-benchmarks-5.4.3.tar.gz
cd osu-micro-benchmarks-5.4.3
./configure CC=/usr/bin/mpicc CXX=/usr/bin/mpicxx
make -j 20
