#!/bin/bash
NCCL_VERSION=2.6
wget -O - https://github.com/NVIDIA/nccl/archive/v$NCCL_VERSION.tar.gz | tar -xzf -
cd nccl-$NCCL_VERSION && make -j src.build && make pkg.txz.build
sudo mkdir -p /usr/local/nccl
sudo tar -Jxf build/pkg/txz/nccl*.txz -C /usr/local/nccl/ --strip-components 1
sudo sh -c 'echo "/usr/local/nccl/lib" >> /etc/ld.so.conf.d/nvidia.conf'
sudo ldconfig
rm -rf nccl-$NCCL_VERSION
