#!/bin/bash

source ./util_functions

if [ $# -eq 0 ]; then
	tip "Usage: $0 <new_user> [new_user_uid]"
	exit 1
fi

NEW_USER=$1

if [ $# -eq 2 ]; then
	NEW_USER_UID=$2
	UID_ARGS="-u $NEW_USER_UID"
else
	UID_ARGS=""
fi

getent passwd $NEW_USER
if [ $? -eq 0 ]; then
	IS_USER_CREATED="yes"
	error "the user $NEW_USER has already been created"
	exit 2
fi

if [ "x$IS_USER_CREATED" != "xyes" ]; then

# add new user
RELEASE_ID=`get_release_id`
if [ "x$RELEASE_ID" = "xubuntu" ]; then
sudo useradd $UID_ARGS -m -g users -G adm,dialout,cdrom,floppy,sudo,audio,dip,video,plugdev,staff,systemd-journal -s /bin/bash $NEW_USER
elif [ "x$RELEASE_ID" = "xcentos" ]; then
sudo useradd $UID_ARGS -m -g users -G adm,dialout,cdrom,floppy,wheel,audio,video,systemd-journal -s /bin/bash $NEW_USER
fi

# set password for the new user
if [ -f passfile ]; then

# non-ascii and ' ' '\r'... are now prohibited in passfile
# it should contains at most 1 '\n' as the last character
password=`cat passfile`

# since the password of ubuntu is empty, so we just need type the new password
# TODO(cjr): chpasswd
echo -ne "$password\n$password\n" | sudo passwd $NEW_USER
#sudo chpasswd $NEW_USER:$password

if [ $? -ne 0 ]; then
	printf "%s\n" "error when setting password"
	exit -1
fi

rm -f passfile
info "user $NEW_USER created successfully"

fi

fi
