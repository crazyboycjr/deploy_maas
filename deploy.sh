#!/bin/bash

# assume the default user is ubuntu
NEW_USER=cjr
NEW_USER_UID=400000

source ./util_functions

if [ $# -eq 1 ]; then
	if [ "x$1" = "xoverwrite" ]; then
		NEW_USER_HOME=`getent passwd $NEW_USER | cut -d ":" -f 6`
		sudo userdel $NEW_USER
		sudo rm -rf $NEW_USER_HOME
	fi
fi

getent passwd $NEW_USER_UID
if [ $? -eq 0 ]; then
	error "UID ${NEW_USER_UID} is not unique, please change to a unique one"
	exit 1
fi

./create_user.sh $NEW_USER $NEW_USER_UID

# make user cjr be able to root without password temporary
echo "$NEW_USER ALL=(ALL) NOPASSWD:ALL" | sudo sh -c "cat - >> /etc/sudoers.d/91-users-$NEW_USER"

# switch to new user without knowing the user's password

DIR=$(dirname `realpath $0`)
DIRNAME=`basename $DIR`
cp -r $DIR /tmp/

RELEASE_ID=`get_release_id`
INSTALL_SCRIPT=`realpath /tmp/$DIRNAME/$RELEASE_ID/install_tools.sh`
# make sure user cjr have access to the install_tools.sh script
echo sudo -iu $NEW_USER -H $INSTALL_SCRIPT $NEW_USER
sudo -iu $NEW_USER -H $INSTALL_SCRIPT $NEW_USER

if [ $? -eq 0 ]; then
info "install tools finished"
else
error "install tools exit with code $?"
fi

# disable nopasswd for user
#sudo rm -f "/etc/sudoers.d/91-users-$NEW_USER"
