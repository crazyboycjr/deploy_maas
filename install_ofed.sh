#!/bin/bash

source ./util_functions

[[ $# -ge 1 ]] && INSTALL_OPTS="$*"

# https://content.mellanox.com/ofed/MLNX_OFED-5.2-1.0.4.0/MLNX_OFED_LINUX-5.2-1.0.4.0-ubuntu20.04-x86_64.tgz

# user /etc/os-release
RELEASE_ID=`get_release_id`
VERSION_ID=`get_version_id`

info "install ofed stack on $RELEASE_ID $VERSION_ID"

# see also https://linux.mellanox.com/public/repo/mlnx_ofed/

#install mellanox driver
# VERSION=4.6-1.0.1.1
# VERSION=5.2-1.0.4.0
# VERSION=5.4-1.0.3.0
VERSION=5.5-1.0.3.2
if [ "x$RELEASE_ID" = "xcentos" ]; then
RELEASE=rhel${VERSION_ID}
else
RELEASE=${RELEASE_ID}${VERSION_ID}
fi
ARCH=x86_64
FILENAME=MLNX_OFED_LINUX-${VERSION}-${RELEASE}-${ARCH}
FILE=${FILENAME}.tgz
wget -nc http://content.mellanox.com/ofed/MLNX_OFED-${VERSION}/$FILE
if [ "x$RELEASE_ID" = "xubuntu" ]; then
wget -nc -qO - http://www.mellanox.com/downloads/ofed/RPM-GPG-KEY-Mellanox | sudo apt-key add -
apt-key list
elif [ "x$RELEASE_ID" = "xcentos" ]; then
wget -nc http://www.mellanox.com/downloads/ofed/RPM-GPG-KEY-Mellanox
sudo rpm --import RPM-GPG-KEY-Mellanox && rm RPM-GPG-KEY-Mellanox
rpm -qa gpg-pubkey\* --qf "%{name}-%{version}-%{release}-%{summary}\n"
fi


tar xf ${FILE}
cd ${FILENAME}

if [ ! -z $INSTALL_OPTS ]; then
	sudo ./mlnxofedinstall $INSTALL_OPTS
else
	# sudo ./mlnxofedinstall --force --add-kernel-support
	sudo ./mlnxofedinstall --force --dkms
fi

sudo systemctl restart openibd.service
ibv_devinfo

if [ $? -ne 0 ]; then
	error "the driver failed to install"
	exit 2
fi

sudo systemctl enable openibd.service
tip "remember to reboot the computer"
