#!/bin/bash

DIR=`mktemp -d`
echo ${DIR}
clush -P -B -l cjr -w @all --rcopy \$HOME/.ssh/id_ed25519.pub --dest ${DIR}

rm -f ${DIR}/all_keys
cat /${DIR}/* >> /${DIR}/all_keys

cp /${DIR}/all_keys ./all_keys
rm -rf ${DIR}
