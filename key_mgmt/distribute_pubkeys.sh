#!/bin/bash

clush -P -B -l cjr -w @all --copy ./all_keys --dest /tmp/keys

clush -P -B -l cjr -w @all "cat /tmp/keys >> \$HOME/.ssh/authorized_keys"
