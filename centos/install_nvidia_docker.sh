#/bin/bash

# Remove old version docker, require docker version >= 19.03
# sudo yum remove -y docker \
# 	docker-client \
# 	docker-client-latest \
# 	docker-common \
# 	docker-latest \
# 	docker-latest-logrotate \
# 	docker-logrotate \
# 	docker-engine

# Install required packages
sudo yum install -y yum-utils \
	device-mapper-persistent-data \
	lvm2

# Set up the stable repository
sudo yum-config-manager \
	--add-repo \
	https://download.docker.com/linux/centos/docker-ce.repo

# Add Docker’s official GPG key:
sudo rpm --import https://download.docker.com/linux/centos/gpg

# rpm -q bash gpg-pubkey --qf '%{Description}' | gpg --with-fingerprint

# Install the latest version of Docker CE and containerd
sudo yum install -y docker-ce docker-ce-cli containerd.io
# or to install a specific version
# apt-cache madison docker-ce
# yum list docker-ce --showduplicates | sort -r
# sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io

# Add current user to docker group
sudo gpasswd -a "$USER" docker

# To test, run hello world
# sudo docker run hello-world

# If you have nvidia-docker 1.0 installed: we need to remove it and all existing GPU containers
# docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
# sudo apt-get purge -y nvidia-docker

# Add the package repositories
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo | sudo tee /etc/yum.repos.d/nvidia-docker.repo

# Install nvidia-docker2 and reload the Docker daemon configuration
sudo yum install -y nvidia-container-toolkit
sudo yum install -y nvidia-docker2 # this is unnecessary for latest docker version >= 19.03
# if this failed to download, run 'yum clean all'
sudo systemctl restart docker

# if selinux is enabled, execute this
if [[ -f "/usr/sbin/getenforce" && "`/usr/sbin/getenforce`" = "Enforcing" ]]; then
sudo semanage fcontext -a -t container_runtime_exec_t /usr/bin/nvidia-docker
sudo restorecon -v /usr/bin/nvidia-docker
fi

# Test nvidia-smi with the latest official CUDA image
#docker run --runtime=nvidia --rm nvidia/cuda:9.0-base nvidia-smi
