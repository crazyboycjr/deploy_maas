# deploy
scripts to deploy working environment on hkust maas cluster

currently, I haven't set up a shared storage for these nodes,
so I just execute these scripts on each machine.
this practice should be changed once our shared storage is ready

# Usage
__edit the passfile__ and execute `deploy.sh` on every machine
```
mv passfile.example passfile
clush -P -B -l ubuntu -w @rack2\!storage01.maas --copy * --dest /home/ubuntu/
python -u /usr/bin/clush -P -l ubuntu -w @rack2\!storage01.maas ./deploy.sh overwrite
python -u /usr/bin/clush -P -l ubuntu -w @rack2\!storage01.maas ./install_ofed.sh #if needed
clush -P -B -l cjr -w @rack2 --copy util_functions install_mpi.sh --dest \$HOME/
clush -P -B -l ubuntu -w @rack2 "echo \"cjr ALL=(ALL) NOPASSWD:ALL\" | sudo sh -c \"cat - >> /etc/sudoers.d/91-users-cjr\"
python -u /usr/bin/clush -P -l cjr -w @rack2 ./install_mpi.sh
cd key_mgmt
./collect_pubkeys.sh
./distribute_pubkeys.sh
```
