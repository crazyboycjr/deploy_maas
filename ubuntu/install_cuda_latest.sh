#!/bin/bash

# Date: 2022-1-2. cuda 11.5, cudnn 8.3.1 by this date
# https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#ubuntu-installation

# make sure linux headers are installed, and previous installation of cuda is cleanly removed

DISTRO=ubuntu2004
ARCH=x86_64

# Add repo metadata
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/$DISTRO/$ARCH/7fa2af80.pub
sudo add-apt-repository "deb http://developer.download.nvidia.com/compute/cuda/repos/$DISTRO/$ARCH/ /"

# Pin file to prioritize official CUDA repository from nvidia
wget -nc https://developer.download.nvidia.com/compute/cuda/repos/$DISTRO/$ARCH/cuda-$DISTRO.pin
sudo install -Dm644 cuda-$DISTRO.pin /etc/apt/preferences.d/cuda-repository-pin-600

# Update package cache and install cuda
sudo apt update
sudo apt install -y cuda

# To include all GDS packages
sudo apt install -y nvidia-gds

# GDR nv_peer_mem
# TODO

# install cuDNN
sudo apt install -y libcudnn8-dev

# install nccl
sudo apt install -y libnccl-dev
