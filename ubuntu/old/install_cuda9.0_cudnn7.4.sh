#!/bin/bash

# install cuda v9.0
wget -nc https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64-deb -O cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu1604-9-0-local_9.0.176-1_amd64.deb
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub
sudo apt-get update
sudo apt-get install -y cuda

# install cuDNN v7.4.2
CUDNN_FILE_NAME=cudnn-9.0-linux-x64-v7.4.2.24.tgz
wget -nc https://developer.download.nvidia.com/compute/redist/cudnn/v7.4.2/$CUDNN_FILE_NAME
tar -xvf $CUDNN_FILE_NAME
sudo cp cuda/include/cudnn.h /usr/local/cuda-9.0/include/cudnn.h
sudo cp cuda/lib64/libcudnn.so.7.4.2 /usr/local/cuda-9.0/lib64/libcudnn.so.7.4.2
sudo ln -sf /usr/local/cuda-9.0/lib64/libcudnn.so.7.4.2 /usr/local/cuda-9.0/lib64/libcudnn.so.7
sudo ln -sf /usr/local/cuda-9.0/lib64/libcudnn.so.7 /usr/local/cuda-9.0/lib64/libcudnn.so

sudo ln -sf /usr/local/cuda-9.0 /usr/local/cuda

# set environment variables
#cat >> ~/.bashrc << EOF
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-10.1/lib64
#export CUDA_HOME=/usr/local/cuda-10.1
#export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/local/cuda-10.1/samples/common/inc
#EOF
