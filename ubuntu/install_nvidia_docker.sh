#/bin/bash

# Install docker
sudo apt-get update

# Install packages to allow apt to use a repository over HTTPS:
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
    
# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add fingerprint
sudo apt-key fingerprint 0EBFCD88

# Install statble repository, and change to the tsinghua mirror instead of docker mirror
if [ "x$REGION" = "xchina" ]; then
sudo add-apt-repository \
   "deb [arch=amd64] https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/ubuntu/ \
   $(lsb_release -cs) \
   stable"
else
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
fi

# Update the apt package index
sudo apt-get update

# Install the latest version of Docker CE and containerd
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
# or to install a specific version
# apt-cache madison docker-ce
# sudo apt-get install -y docker-ce=<VERSION_STRING> docker-ce-cli=<VERSION_STRING> containerd.io

# Add current user to docker group
sudo gpasswd -a "$USER" docker

# To test, run hello world
# sudo docker run hello-world

# If you have nvidia-docker 1.0 installed: we need to remove it and all existing GPU containers
# docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
# sudo apt-get purge -y nvidia-docker

# Add the package repositories
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
  sudo apt-key add -
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
  sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt-get update

# Install nvidia-docker2 and reload the Docker daemon configuration
sudo apt-get install -y nvidia-docker2
sudo pkill -SIGHUP dockerd

# Test nvidia-smi with the latest official CUDA image
#docker run --runtime=nvidia --rm nvidia/cuda:9.0-base nvidia-smi
